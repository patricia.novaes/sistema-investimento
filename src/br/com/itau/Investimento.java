package br.com.itau;

public class Investimento {

    private double valor;
    private int tempoInvestimento;
    public double juros = 0.7D;

    public Investimento(double valor, int tempoInvestimento) {
        this.valor = valor;
        this.tempoInvestimento = tempoInvestimento;
    }

    public Investimento () {}


    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getTempoInvestimento() {
        return tempoInvestimento;
    }

    public void setTempoInvestimento(int tempoInvestimento) {
        this.tempoInvestimento = tempoInvestimento;
    }

    public double getJuros() {
        return juros;
    }


}
