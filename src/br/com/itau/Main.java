package br.com.itau;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Investimento investimento = new Investimento();

        System.out.print("Insira o valor que deseja investir: ");
        investimento.setValor(input.nextDouble());

        System.out.print("Insira por quantos meses deseja investir o valor informado: ");
        investimento.setTempoInvestimento(input.nextInt());

        Calculadora calculoDoInvestimento = new Calculadora();
        Double resultadoInvestimento = calculoDoInvestimento.calcularInvestimento(investimento);

        Impressora.informarValorInvestimento(resultadoInvestimento);

    }
}
