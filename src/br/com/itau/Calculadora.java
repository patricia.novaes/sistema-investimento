package br.com.itau;

public class Calculadora {

    private double resultadoInvestimento;
    private double calculo;

    public double calcularInvestimento (Investimento dadosInvestimento){
        calculo = (dadosInvestimento.getValor() * dadosInvestimento.getJuros()) / 100;
        resultadoInvestimento = (calculo * dadosInvestimento.getTempoInvestimento()) + dadosInvestimento.getValor();
        return this.resultadoInvestimento;
    }

    public double getResultadoInvestimento() {
        return resultadoInvestimento;
    }

}
